//aqui definimos as regras de negócio do sistema (controller)

//dentro de uma classe um atributo seria o mesmo que uma variável e um método seria o mesmo que uma função
//porém todos dentro do escopo de uma classe

//atributo é uma espécie de variável com mais recursos, com isso é possível chamar a variável 
//metodo é uma ação PS: se for um código reaproveitável em vários lugares, isso caracteriza como o uso/criação de metodos

//referência em várias partes do código (usando 'this' ao invés do nome de uma var, fazendo referência ao escopo da classe)
/*encapsulamento controla quem pode acessar determinado atributo ou método 
(garante a segurança das regras de negócio e de algo que não pode alterar dentro do projeto)
3 maneiras: private = só atributos da mesma classe podem chamar (dentro do mesmo escopo de uma função = classe)
            protected = atributos e métodos da mesma classe e classe pai podem chamar
            public = tá tudo liberado hehe
*/

class CalcController {

    constructor() {

        //se o atributo começar com '_' será um atributo do tipo private ex: this._displayCalc = ...
        this._operation = [];
        this._locale = 'pt-BR';
        this._displayCalcEl = document.querySelector('#display');
        this._dateEl = document.querySelector('#data');
        this._timeEl = document.querySelector('#hora');
        this._displayCalc = '0';
        this._currentDate;
        this.intialize();
        this.initButtonsEvents();
    }

    //tudo que for acontentecer na calculadora deve ser colocado dentro do initialize (esse metodo inicia tudo dentro de seu escopo automaticamente)
    intialize() {
        //incializando o relogio antes para corrigir o bug do atraso de 1seg para renderizar na tela
        this.setDisplayDateTime();

        //setInterval e uma funçao nativa para executar determinada ação em um tempo pré programado em milisegundos (continuamente)
        //(no caso iremos usar no relógio) 

        setInterval(() => {

            this.setDisplayDateTime();

        }, 1000);

        //setTimeOut é uma função nativa para executar algo uma unica vez depois de um tempo pré-determinado
        //    setTimeout(()=>{
        //        //limpa o intervalo da função set e para de executar a mesma (guardei a set numa variável pra poder manipular)
        //         clearInterval(interval);
        //    },10000);

        /** Para simplificar a criação de função podemos usar 'Arrow Function, novo recurso do ES5/6 que retira a 
         * criação da função declarando como 'function(){regras...}'  podendo ser simplesmente '()=>'**/

        // //exemplo:
        // antigo
        // setInterval(function(){

        // },1000);
        //novo
        // setInterval(()=>{

        // },1000);

    }

    /***O metodo addEventListenerAll foi criada para podermos passar diversos eventos de clique no botao
     * Como o JS só permite um evento por click podemos facilitar dessa maneira, passando como parametros
     * o elemento a ser clicado (no caso a classe '.btn', o evento (click, drag, mouseover, etc) e a função (fn)
     * a ser executada.
     * 
     * Dentro do metodo usamos o split() para separar as strings por algum caractere (nesse caso somente por espaço)
     * e depois percoremos o array com o forEach e aplicamos o evento de fato para cada botão.
    */

    addEventListenerAll(element, events, fn) {

        events.split(' ').forEach(event => {

            element.addEventListener(event, fn, false);

        });

    }


    /****************************
     * Operações da calculadora 
     * **************************/

    clearAll() {
        //metodo para limpar o botão
        this._operation = [];
        console.log(this._operation);
        var clearValue = 0;
        this.showOperation(clearValue);
    }

    clearEntry() {
        //metodo para limpar a ultima operação
        //pop limpa o ultimo item do array
        this._operation.pop();
        console.log(this._operation);
    }

    getLastOperation() {
        return this._operation[this._operation.length - 1];
    }

    setLastOperation(value) {
        this._operation[this._operation.length - 1] = value;
    }

    //verifica o tipo de operação que foi selecionado dentro desse Array
    isOperator(value) {
        return (['+', '-', '*', '%', '/'].indexOf(value) > -1);
    }

    //metodo abaixo criado por mim (ainda não foi adicionado pelo conteudo do curso)
    showOperation(value){
         this.displayCalc = value; // linha para escrever o numero no HTML (adicionado por mim)
    }

    addOperation(value) {

        console.log("A", isNaN(this.getLastOperation()));

        /**a função isNaN (not a number) verifica se a entra da variável é um numero ou string,
         * caso seja uma string, retorna TRUE, se for um número irá retornar FALSE
        */
        if (isNaN(this.getLastOperation())) {
           
            if (this.isOperator(value)) {
                //trocar o operador (caso selecione por exemplo uma adição e depois perceba que precisa mudar para divisão)
                this._setLastOperation(value);

            } else if(isNaN(value)) {
                // se não for numero faça outra coisa
                console.log('não e numero');

            } else{
                this._operation.push(value);
                this.showOperation(value);
            }
            
        }

        else {

            /*
             * no newValue, estamos transformando o ultimo valor do botão em string e concatenando com o value capturado
             * para formar um unico numero (vc giditou 50 e depois 3, 
             * para não adicionar em uma posição diferente do array é necessário isso, assim teremos 503 numa unica posição do array),
             * após isso fazemos um push com o novo valor concatenado
             * 
             */

            let newValue = this.getLastOperation().toString() + value.toString();
            this.setLastOperation(parseInt(newValue)); //converte o novo array concatenado para inteiro novamente
            this.showOperation(newValue);
        
        }

        
        console.log(this._operation);

    }

    setError() {
        this.displayCalc = 'Error';
    }

    execBtn(value) {
        switch (value) {
            case 'ac':
                this.clearAll();
                break;

            case 'ce':
                this.clearEntry();
                break;

            case 'soma':
                //ação
                break;

            case 'subtracao':
                //ação
                break;

            case 'divisao':
                //ação
                break;

            case 'multiplicacao':
                //ação
                break;

            case 'porcento':
                //ação
                break;

            case 'igual':
                //ação
                break;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                this.addOperation(parseInt(value)); //parseInt para converter string em inteiro
                break;

            default:
                this.setError();
                break;
        }
    }

    /****************************
    * fim Operações da calculadora 
    * **************************/







    initButtonsEvents() {
        let buttons = document.querySelectorAll('#buttons > g, #parts > g');

        buttons.forEach((btn, index) => {

            //'e' é o parametro da function ex: function (e){...}
            this.addEventListenerAll(btn, 'click drag', e => {
                //acessou todo html e limpou só pelo nome da classe (o baseval foi usado pq é um svg)
                //com o replace tiramos o 'btn-' da classe e substituimos por ''(vazio)
                console.log(btn.className.baseVal.replace('btn-', ''));

                let textBtn = btn.className.baseVal.replace("btn-", "");

                this.execBtn(textBtn);


            });

            //somente para mudar o cursor quando passar o mouse em cima do elemento(botão)
            this.addEventListenerAll(btn, 'mouseover mouseup mousedown', e => {
                btn.style.cursor = 'pointer';
            });

        });
    }

    //metodo para a data e hora na calculadora
    setDisplayDateTime() {
        this.displayDate = this.currentDate.toLocaleDateString(this._locale, {
            day: '2-digit', //personalizar a exibição do dia
            month: 'short', //long para o mes inteiro e short para abreviado
            year: 'numeric',
        });

        this.displayTime = this.currentDate.toLocaleTimeString(this._locale);
    }

    get displayTime() {
        return this._timeEl.innerHTML;
    }

    set displayTime(horario) {
        this._timeEl.innerHTML = horario;
    }

    get displayDate() {
        return this._dateEl.innerHTML;
    }

    set displayDate(data) {
        this._dateEl.innerHTML = data;
    }

    //gettering (get) e settering (set) são usados para definir o acesso dos atributos e métodos privados
    get displayCalc() {
        return this._displayCalc;
    }

    set displayCalc(valor) {
        this._displayCalcEl.innerHTML = valor;
    }

    get currentDate() {
        return new Date();
    }

    //set para atribuir um valor e guardar no atributo encapsulado (no caso por exemplo de armazenar um valor ao digitar um numero no teclado)
    set currentDate(valor) {
        this._currentDate = valor;
    }
}