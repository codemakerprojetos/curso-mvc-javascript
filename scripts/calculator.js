//para chamar uma classe é necessário instanciar a chamada da classe 
//(representando a classe e usando as regras de negócio dessa classe que está na camada CONTROLLER )
//quando passa o parametro window, quer dizer que essa classe será de acesso global (verificar novamente no vídeo, informação em dúvida)
window.calculator = new CalcController;

//new Date() é uma função nativa para manipular data e local
// calculator.displayDate = new Date().toLocaleDateString('pt-BR');
// calculator.displayTime = new Date().toLocaleTimeString('pt-BR');